package com.example.d308mobiledevelopmentapplicationandroid.UI;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.d308mobiledevelopmentapplicationandroid.R;
import com.example.d308mobiledevelopmentapplicationandroid.entities.Vacation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Project: Vacation Scheduler
 * Package: com.example.d308mobiledevelopmentapplicationandroid.Dao
 * <p>
 * User: Dawood Ahmed
 * Date: 6/14/2023
 * Time: 4:04 PM
 * <p>
 * Created with Android Studio
 * To change this template use File | Settings | File Templates.
 */
public class VacationAdapter extends RecyclerView.Adapter<VacationAdapter.VacationViewHolder> {
    class VacationViewHolder extends RecyclerView.ViewHolder {
        private final TextView vacationItemView, textViewStartDate, textViewEndDate;

        private VacationViewHolder(View itemview) {
            super(itemview);
            vacationItemView = itemview.findViewById(R.id.textView2);
            textViewStartDate = itemview.findViewById(R.id.textViewStartDate);
            textViewEndDate = itemview.findViewById(R.id.textViewEndDate);
            itemview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    final Vacation current = mVacations.get(position);
                    Intent intent = new Intent(context, VacationDetails.class);
                    intent.putExtra("id", current.getVacationID());
                    intent.putExtra("title", current.getVacationTitle());
                    intent.putExtra("hotel", current.getVacationHotel());
                    intent.putExtra("startDate", current.getVacationStartDate());
                    intent.putExtra("endDate", current.getVacationEndDate());
                    context.startActivity(intent);
                }
            });
        }
    }

    private List<Vacation> mVacations;
    private final Context context;
    private final LayoutInflater mInflater;

    public VacationAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public VacationAdapter.VacationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.vacation_list_item, parent, false);
        return new VacationViewHolder((itemView));
    }

    @Override
    public void onBindViewHolder(@NonNull VacationAdapter.VacationViewHolder holder, int position) {
        try {
            if (mVacations != null) {
                Vacation current = mVacations.get(position);
                String name = current.getVacationTitle();
                String startDate = current.getVacationStartDate();
                String endDate = current.getVacationEndDate();
                SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
                Date currentDateFormatted = null, date1, date2;
                try {
                    Date currentDate = new Date();
    //                currentDateFormatted = formatter.parse(currentDate.toString());
                    SimpleDateFormat inputFormatter = new SimpleDateFormat("EEE MMM d HH:mm:ss zzz yyyy");
                    SimpleDateFormat outputFormatter = new SimpleDateFormat("MM/dd/yyyy");
                    try {
                        currentDateFormatted = inputFormatter.parse(currentDate.toString());
                        // Format the Date object into the desired output format using the outputFormatter
                        String outputDateStr = outputFormatter.format(currentDateFormatted);
                    } catch (ParseException e) {
                        System.out.println("Error occurred while parsing the date: " + e.getMessage());
                    }
                    date1 = formatter.parse(startDate.toString());
                    date2 = formatter.parse(endDate.toString());
                } catch (ParseException e) {
                    throw new RuntimeException(e);
                }
                if (currentDateFormatted.getTime() > date1.getTime() == true) {
                    holder.vacationItemView.setText(name + " ending " + endDate);
                } else {
                    holder.vacationItemView.setText(name + " starting " + startDate);
                }

                holder.textViewStartDate.setVisibility(View.GONE);
                holder.textViewEndDate.setVisibility(View.GONE);
    //            holder.textViewStartDate.setText(startDate);
    //            holder.textViewEndDate.setText(endDate);
            } else {

                holder.textViewStartDate.setVisibility(View.GONE);
                holder.textViewEndDate.setVisibility(View.GONE);
                holder.vacationItemView.setText("No Vacation Title");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mVacations.size();
    }

    public void setVacations(List<Vacation> vacations) {
        mVacations = vacations;
        notifyDataSetChanged();
    }
}