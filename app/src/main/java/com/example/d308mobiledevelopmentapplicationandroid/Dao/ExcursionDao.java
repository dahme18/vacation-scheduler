package com.example.d308mobiledevelopmentapplicationandroid.Dao;


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.d308mobiledevelopmentapplicationandroid.entities.Excursion;

import java.util.List;

/**
 * Project: Vacation Scheduler
 * Package: com.example.d308mobiledevelopmentapplicationandroid.Dao
 * <p>
 * User: Dawood Ahmed
 * Date: 6/14/2023
 * Time: 4:04 PM
 * <p>
 * Created with Android Studio
 * To change this template use File | Settings | File Templates.
 */
@Dao
public interface ExcursionDao {

    @Insert(onConflict= OnConflictStrategy.IGNORE)
    void insert(Excursion excursion);
    @Update
    void update(Excursion excursion);
    @Delete
    void delete(Excursion excursion);

    @Query("SELECT * FROM EXCURSIONS ORDER BY excursionID ASC")
    List<Excursion> getAllExcursions();

    @Query("SELECT * FROM EXCURSIONS WHERE vacationID= :vacationID ORDER BY excursionID ASC")
    List<Excursion> getAllAssociatedExcursions(int vacationID);

    @Query("SELECT * FROM EXCURSIONS WHERE vacationID= :vacationID ORDER BY excursionID ASC")
    List<Excursion> getAllAssociatadExcursions(int vacationID);
}
