package com.example.d308mobiledevelopmentapplicationandroid.Database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.d308mobiledevelopmentapplicationandroid.Dao.ExcursionDao;
import com.example.d308mobiledevelopmentapplicationandroid.Dao.VacationDao;
import com.example.d308mobiledevelopmentapplicationandroid.entities.Excursion;
import com.example.d308mobiledevelopmentapplicationandroid.entities.Vacation;

/**
 * Project: Vacation Scheduler
 * Package: com.example.d308mobiledevelopmentapplicationandroid.Dao
 * <p>
 * User: Dawood Ahmed
 * Date: 6/14/2023
 * Time: 4:04 PM
 * <p>
 * Created with Android Studio
 * To change this template use File | Settings | File Templates.
 */
@Database(entities = {Vacation.class, Excursion.class}, version=1, exportSchema = false)
public abstract class VacationDatabaseBuilder extends RoomDatabase {
    public abstract VacationDao vacationDao();
    public abstract ExcursionDao excursionDao();

    private static  volatile  VacationDatabaseBuilder INSTANCE;

    static VacationDatabaseBuilder getDatabase(final Context context){
        if(INSTANCE==null){
            synchronized (VacationDatabaseBuilder.class){
                if (INSTANCE==null){
                    INSTANCE= Room.databaseBuilder(context.getApplicationContext(),VacationDatabaseBuilder.class,"MyVacationDatabase.db")
                            .fallbackToDestructiveMigration()
                            .build();
                }

            }

        }
        return INSTANCE;
    }
}
