/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.example.d308mobiledevelopmentapplicationandroid.Database;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "android.d308mobiledevelopmentapplicationandroid.Database";
  public static final String BUILD_TYPE = "debug";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0";
}
